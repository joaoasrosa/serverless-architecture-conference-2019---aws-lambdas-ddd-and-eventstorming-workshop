using System;
using System.Collections.Generic;
using System.Net;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.TestUtilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Xunit;

namespace Adapters.Api.ConfirmEmployee.Tests
{
    public class WhenEmployeeParks
    {
        [Fact]
        public void GivenThereAreSpacesAvailable_ThenParkingSpaceIs199()
        {
            var parkingGarageId = CreateData();
            SetEnvironmentVariables();

            Functions functions = new Functions();

            var request = new APIGatewayProxyRequest
            {
                PathParameters = new Dictionary<string, string>
                {
                    {
                        "parkingGarageId",
                        parkingGarageId.ToString()
                    }
                }
            };

            var context = new TestLambdaContext();
            var response = functions.Post(request, context);

            var employeeParkingConfirmation = JsonConvert.DeserializeObject<EmployeeParkingConfirmation>(response.Body);

            Assert.Equal(200, response.StatusCode);
            Assert.Equal(parkingGarageId, employeeParkingConfirmation.Id);
            Assert.Equal((uint)199, employeeParkingConfirmation.Free);
        }

        private Guid CreateData()
        {
            using (var dynamoDbClient = CreateDynamoDbClient())
            {
                const string tableName = "ParkingGarage";

                var tables = dynamoDbClient.ListTablesAsync().Result;
                if (tables.TableNames.Count > 0)
                {
                    var deleteTableResponse = dynamoDbClient.DeleteTableAsync(tableName).Result;
                    if (deleteTableResponse.HttpStatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception($"Can't delete the DynamoDB table ${tableName}.");
                    }
                }

                dynamoDbClient.CreateTableAsync(tableName,
                    new List<KeySchemaElement>
                    {
                        new KeySchemaElement(nameof(Persistence.DataEntities.ParkingGarage.Id), KeyType.HASH)
                    },
                    new List<AttributeDefinition>
                    {
                        new AttributeDefinition(nameof(Persistence.DataEntities.ParkingGarage.Id),
                            ScalarAttributeType.S)
                    },
                    new ProvisionedThroughput(5, 5));

                using (var dynamoDbContext = new DynamoDBContext(dynamoDbClient))
                {
                    var parkingGarageId = Guid.NewGuid();

                    dynamoDbContext.SaveAsync(
                        new Persistence.DataEntities.ParkingGarage
                        {
                            Id = parkingGarageId,
                            Day = DateTime.UtcNow.Date,
                            Free = 200,
                            Capacity = 200
                        }).Wait();

                    return parkingGarageId;
                }
            }
        }

        private static IAmazonDynamoDB CreateDynamoDbClient()
        {
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile("appsettings.json");

            var configuration = configurationBuilder.Build();

            var options = configuration.GetAWSOptions();
            return options.CreateServiceClient<IAmazonDynamoDB>();
        }

        private static void SetEnvironmentVariables()
        {
            Environment.SetEnvironmentVariable("AWS__ServiceURL", "http://localhost:8000");
        }
    }
}

using System;

namespace Adapters.Api.ConfirmEmployee.Tests
{
    public class EmployeeParkingConfirmation
    {
        public Guid Id { get; set; }

        public DateTime Day { get; set; }

        public uint Capacity { get; set; }

        public uint Free { get; set; }
    }
}

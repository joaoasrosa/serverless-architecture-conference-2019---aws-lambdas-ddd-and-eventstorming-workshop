using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using Adapters.Persistence;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Domain.ParkingGarage;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Adapters.Api.ConfirmEmployee
{
    public class Functions
    {
        /// <summary>
        /// A Lambda function to respond to HTTP POST methods from API Gateway
        /// </summary>
        /// <param name="request">The API Gateway request</param>
        /// <param name="context">The execution context</param>
        /// <returns>The parking garage</returns>
        public APIGatewayProxyResponse Post(APIGatewayProxyRequest request, ILambdaContext context)
        {
            if (!request.PathParameters.ContainsKey("parkingGarageId"))
            {
                context.Logger.LogLine("WARNING: Missing parking garage id");
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.BadRequest
                };
            }

            try
            {
                context.Logger.LogLine("Confirm employee parking.\n");

                var employeeService = CreateEmployeeService();
                var parkingGarage = employeeService.OccupyParkingSpace(
                    (ParkingGarageId)request.PathParameters["parkingGarageId"]
                );

                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Body = JsonConvert.SerializeObject(new
                    {
                        Id = (Guid)parkingGarage.Id,
                        Day = parkingGarage.Day,
                        Capacity = (uint)parkingGarage.CapacityParkingSpace(),
                        Free = (uint)parkingGarage.CurrentlyAvailable()
                    }),
                    Headers = new Dictionary<string, string> {{"Content-Type", "application/json"}}
                };

                return response;
            }
            catch (Exception exception)
            {
                context.Logger.LogLine($"ERROR: {exception.Message}");
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError
                };
            }
        }

        private static EmployeeService CreateEmployeeService()
        {
            var parkingGarageRepositoryAdapter = CreateParkingGarageRepositoryAdapter();

            return new EmployeeService(parkingGarageRepositoryAdapter);
        }

        private static ParkingGarageRepositoryAdapter CreateParkingGarageRepositoryAdapter()
        {
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddEnvironmentVariables();

            var configuration = configurationBuilder.Build();

            var dynamoDbClientFactory = DynamoDbClientFactory.CreateFrom(
                configuration
            );

            return new ParkingGarageRepositoryAdapter(
                dynamoDbClientFactory
            );
        }
    }
}

﻿using System;
using Amazon.DynamoDBv2.DataModel;
using Domain.ParkingGarage;

namespace Adapters.Persistence
{
    public class ParkingGarageRepositoryAdapter : IParkingGarageRepositoryAdapter
    {
        private readonly IDynamoDbClientFactory _dynamoDbClientFactory;

        public ParkingGarageRepositoryAdapter(IDynamoDbClientFactory dynamoDbClientFactory)
        {
            _dynamoDbClientFactory =
                dynamoDbClientFactory ?? throw new ArgumentNullException(nameof(dynamoDbClientFactory));
        }

        public void SaveParkingGarage(ParkingGarage parkingGarage)
        {
            using (var dynamoDbClient = _dynamoDbClientFactory.Create())
            {
                using (var dynamoDbContext = new DynamoDBContext(dynamoDbClient))
                {
                    dynamoDbContext.SaveAsync(new DataEntities.ParkingGarage(parkingGarage)).Wait();
                }
            }
        }

        public ParkingGarage FindParkingGarage(ParkingGarageId id)
        {
            using (var dynamoDbClient = _dynamoDbClientFactory.Create())
            {
                using (var dynamoDbContext = new DynamoDBContext(dynamoDbClient))
                {
                    var parkingGarage = dynamoDbContext.LoadAsync<DataEntities.ParkingGarage>((Guid)id).Result;
                    return parkingGarage.AsDomainModel();
                }
            }
        }
    }
}

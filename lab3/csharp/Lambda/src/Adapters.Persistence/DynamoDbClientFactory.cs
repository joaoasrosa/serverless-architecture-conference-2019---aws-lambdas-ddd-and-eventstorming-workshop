using System;
using Amazon.DynamoDBv2;
using Microsoft.Extensions.Configuration;

namespace Adapters.Persistence
{
    public interface IDynamoDbClientFactory
    {
        IAmazonDynamoDB Create();
    }

    public class DynamoDbClientFactory : IDynamoDbClientFactory
    {
        private readonly IConfiguration _configuration;

        private DynamoDbClientFactory(
            IConfiguration configuration
        )
        {
            _configuration =
                configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public IAmazonDynamoDB Create()
        {
            var options = _configuration.GetAWSOptions();
            return options.CreateServiceClient<IAmazonDynamoDB>();
        }

        public static DynamoDbClientFactory CreateFrom(
            IConfiguration configuration
        )
        {
            return new DynamoDbClientFactory(
                configuration
            );
        }
    }
}

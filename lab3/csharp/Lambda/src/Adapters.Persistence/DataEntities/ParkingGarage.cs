using System;
using Amazon.DynamoDBv2.DataModel;
using Domain.ParkingGarage;

namespace Adapters.Persistence.DataEntities
{
    [DynamoDBTable("ParkingGarage")]
    public class ParkingGarage
    {
        public ParkingGarage(Domain.ParkingGarage.ParkingGarage parkingGarage)
        {
            Id = parkingGarage.Id;
            Day = parkingGarage.Day;
            Capacity = parkingGarage.CapacityParkingSpace();
            Free = parkingGarage.CurrentlyAvailable();
        }

        public ParkingGarage()
        {
        }

        [DynamoDBHashKey] public Guid Id { get; set; }

        [DynamoDBProperty] public DateTime Day { get; set; }

        [DynamoDBProperty] public uint Capacity { get; set; }

        [DynamoDBProperty] public uint Free { get; set; }

        internal Domain.ParkingGarage.ParkingGarage AsDomainModel()
        {
            return new Domain.ParkingGarage.ParkingGarage(
                (ParkingGarageId)Id,
                Day,
                Capacity,
                Free
            );
        }
    }
}

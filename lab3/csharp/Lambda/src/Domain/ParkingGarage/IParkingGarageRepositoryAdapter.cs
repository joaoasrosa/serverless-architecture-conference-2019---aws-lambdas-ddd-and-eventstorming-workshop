namespace Domain.ParkingGarage
{
    public interface IParkingGarageRepositoryAdapter
    {
        void SaveParkingGarage(ParkingGarage parkingGarage);

        ParkingGarage FindParkingGarage(ParkingGarageId id);
    }
}

using System;

namespace Domain.ParkingGarage
{
    public class EmployeeService
    {
        private readonly IParkingGarageRepositoryAdapter _parkingGarageRepositoryAdapter;

        public EmployeeService(IParkingGarageRepositoryAdapter parkingGarageRepositoryAdapter)
        {
            _parkingGarageRepositoryAdapter = parkingGarageRepositoryAdapter ??
                                              throw new ArgumentNullException(nameof(parkingGarageRepositoryAdapter));
        }

        public ParkingGarage OccupyParkingSpace(ParkingGarageId id)
        {
            var parkingGarage = _parkingGarageRepositoryAdapter.FindParkingGarage(id);
            parkingGarage.OccupyParkingSpace();

            _parkingGarageRepositoryAdapter.SaveParkingGarage(parkingGarage);
            return parkingGarage;
        }
    }
}

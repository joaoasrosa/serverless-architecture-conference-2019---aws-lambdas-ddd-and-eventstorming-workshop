using System;

namespace Domain.ParkingGarage
{
    public struct ParkingGarageId
    {
        private readonly Guid _value;

        private ParkingGarageId(Guid capacity)
        {
            _value = capacity;
        }

        public static implicit operator Guid(ParkingGarageId parkingGarageId)
        {
            return parkingGarageId._value;
        }

        public static explicit operator ParkingGarageId(Guid parkingGarageId)
        {
            return new ParkingGarageId(parkingGarageId);
        }

        public static explicit operator ParkingGarageId(string parkingGarageId)
        {
            return new ParkingGarageId(Guid.Parse(parkingGarageId));
        }
    }
}

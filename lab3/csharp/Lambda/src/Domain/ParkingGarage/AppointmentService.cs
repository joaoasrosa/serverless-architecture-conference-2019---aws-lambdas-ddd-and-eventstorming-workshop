using System;

namespace Domain.ParkingGarage
{
    public class AppointmentService
    {
        private readonly IParkingGarageRepositoryAdapter _parkingGarageRepositoryAdapter;

        public AppointmentService(IParkingGarageRepositoryAdapter parkingGarageRepositoryAdapter)
        {
            _parkingGarageRepositoryAdapter = parkingGarageRepositoryAdapter ??
                                              throw new ArgumentNullException(nameof(parkingGarageRepositoryAdapter));
        }

        public ParkingGarage OccupyParkingSpace(ParkingGarageId id)
        {
            var parkingGarage = _parkingGarageRepositoryAdapter.FindParkingGarage(id);
            parkingGarage.OccupyParkingSpace();
            
            // TODO we have not allocated versioning yet, not needed for example for now

            _parkingGarageRepositoryAdapter.SaveParkingGarage(parkingGarage);
            return parkingGarage;
        }
    }
}

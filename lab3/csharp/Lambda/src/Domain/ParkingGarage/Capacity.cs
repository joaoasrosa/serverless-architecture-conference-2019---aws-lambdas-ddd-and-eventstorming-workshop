using System;

namespace Domain.ParkingGarage
{
    public struct Capacity : IEquatable<Capacity>
    {
        private readonly uint _value;

        public Capacity(uint capacity)
        {
            _value = capacity;
        }

        public static implicit operator uint(Capacity capacity)
        {
            return capacity._value;
        }

        public static explicit operator Capacity(uint capacity)
        {
            return new Capacity(capacity);
        }

        public bool Equals(Capacity other)
        {
            return _value == other._value;
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
                return false;

            return obj is Capacity capacity && Equals(capacity);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public static bool operator ==(Capacity left, Capacity right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Capacity left, Capacity right)
        {
            return !left.Equals(right);
        }

        public static long operator -(Capacity left, Capacity right)
        {
            return left._value - right._value;
        }

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}

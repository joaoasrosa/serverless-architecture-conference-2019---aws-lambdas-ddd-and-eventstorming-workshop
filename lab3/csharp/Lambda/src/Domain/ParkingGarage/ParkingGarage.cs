using System;

namespace Domain.ParkingGarage
{
    public class ParkingGarage
    {
        public ParkingGarageId Id { get; }
        public DateTime Day { get; }
        
        private ParkingSpaces _parkingSpaces;

        public ParkingGarage(ParkingGarageId id, DateTime day, uint capacity, uint free)
        {
            Id = id;
            Day = day;
            
            _parkingSpaces = new ParkingSpaces(new Capacity(capacity), new Capacity(free));
        }

        public Capacity CapacityParkingSpace()
        {
            return _parkingSpaces.Capacity();
        }
        
        public Capacity CurrentlyAvailable()
        {
            return _parkingSpaces.CurrentlyAvailable();
        }

        public void ReserveParkingSpace()
        {
            _parkingSpaces = _parkingSpaces.OccupySpace();
        }

        public void OccupyParkingSpace()
        {
            _parkingSpaces = _parkingSpaces.OccupySpace();
        }

        public void FreeParkingSpace()
        {
            _parkingSpaces = _parkingSpaces.FreeSpace();
        }
    }
}

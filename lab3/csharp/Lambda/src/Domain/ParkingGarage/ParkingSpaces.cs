using System;

namespace Domain.ParkingGarage
{
    public class ParkingSpaces
    {
        private readonly Capacity _capacity;
        private readonly Capacity _free;

        public ParkingSpaces(Capacity capacity, Capacity free)
        {
            _capacity = capacity;
            _free = free;
        }

        public Capacity Capacity()
        {
            return _capacity;
        }

        public Capacity CurrentlyAvailable()
        {
            return _free;
        }

        public ParkingSpaces OccupySpace()
        {
            return new ParkingSpaces(_capacity, new Capacity(_free - 1));
        }

        public ParkingSpaces FreeSpace()
        {
            return new ParkingSpaces(_capacity, new Capacity(_free + 1));
        }
    }
}

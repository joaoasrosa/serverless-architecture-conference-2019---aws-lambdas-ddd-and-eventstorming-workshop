import {ParkingGarage} from "./parking-garage";

export interface ParkingGarageRepository {
    saveParkingGarage(parkingGarage: ParkingGarage): Promise<ParkingGarage>
    findParkingGarage(id: string): Promise<ParkingGarage>
}
import {ParkingGarage, UUID} from "./parking-garage";
import {ParkingGarageRepository} from "./parking-garage-repository";

export class EmployeeService {

    constructor(private parkingGarageRepository: ParkingGarageRepository) {
    }

    public async occupyParkingSpace(parkingSpaceId: UUID): Promise<ParkingGarage> {
        let parkingGarage = await this.parkingGarageRepository.findParkingGarage(parkingSpaceId.id);
        parkingGarage.occupyParkingSpace();
        return await this.parkingGarageRepository.saveParkingGarage(parkingGarage);
    }
}

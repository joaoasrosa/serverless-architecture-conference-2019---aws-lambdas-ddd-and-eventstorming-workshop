import {ParkingGarage, UUID} from "./parking-garage";
import {ParkingGarageRepository} from "./parking-garage-repository";

export class AppointmentService {

    constructor(private parkingGarageRepository: ParkingGarageRepository) {
    }

    public async occupyParkingSpace(parkingSpaceId: UUID): Promise<ParkingGarage> {
        let parkingGarage = await this.parkingGarageRepository.findParkingGarage(parkingSpaceId.id);
        parkingGarage.occupyParkingSpace();
        // TODO we have not allocated versioning yet, not needed for example for now
        return await this.parkingGarageRepository.saveParkingGarage(parkingGarage);
    }
}
import { v4 as uuid } from 'uuid';

export class ParkingGarage {
    parkingSpaces: ParkingSpaces;

    constructor(public readonly id: UUID, public readonly day: string, public readonly capacity: number, public readonly free: number) {
        this.parkingSpaces = new ParkingSpaces(new Capacity(capacity), new Capacity(free));
    }

    public reserveParkingSpace() {
        this.parkingSpaces = this.parkingSpaces.occupySpace();
    }

    public occupyParkingSpace() {
        this.parkingSpaces = this.parkingSpaces.occupySpace();
    }

    public freeParkingSpace() {
        this.parkingSpaces = this.parkingSpaces.freeSpace();
    }
}

export class UUID {
    static new = () => new UUID(uuid());

    constructor(public readonly id: string) {
    }
}

class ParkingSpaces {

    constructor(public readonly capacity: Capacity, public free: Capacity) {
    }

    currentlyAvailable(): Capacity {
        return this.free;
    }

    occupySpace(): ParkingSpaces {
        return new ParkingSpaces(this.capacity, new Capacity(this.free.capacity - 1));
    }

    freeSpace(): ParkingSpaces {
        return new ParkingSpaces(this.capacity, new Capacity(this.free.capacity + 1));
    }
}

class Capacity {

    constructor(public readonly capacity: number) {
    }
}
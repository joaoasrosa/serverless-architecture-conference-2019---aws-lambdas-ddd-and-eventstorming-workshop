export class LicensePlateRegistration {
    constructor(public readonly license: string, public readonly type: LicensePlateType) {
    }
}

export enum LicensePlateType {
    EMPLOYEE,
    APPOINTMENT,
    UNKNOWN
}
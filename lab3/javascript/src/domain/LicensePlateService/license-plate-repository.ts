import {LicensePlateRegistration} from "./license-plate-registration";

export interface LicensePlateRepository {
    findLicensePlate(license: string): Promise<LicensePlateRegistration>;
}
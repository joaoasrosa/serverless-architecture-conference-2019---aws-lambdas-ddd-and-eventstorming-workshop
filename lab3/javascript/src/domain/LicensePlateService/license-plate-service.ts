import {LicensePlateRegistration} from "./license-plate-registration";
import {LicensePlateRepository} from "./license-plate-repository";

export class LicensePlateService {

    constructor(public licensePlateRepository: LicensePlateRepository) {
    }

    public async findMatchingLicensePlate(license: string): Promise<LicensePlateRegistration> {
        console.log("findMatchingLicensePlate");
        return this.licensePlateRepository.findLicensePlate(license);
    }
}
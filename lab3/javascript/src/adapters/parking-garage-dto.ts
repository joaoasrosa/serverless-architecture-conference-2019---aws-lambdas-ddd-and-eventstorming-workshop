import {Serialised, TinyType} from "tiny-types";
import {ParkingGarage, UUID} from "../domain/ParkingGarage/parking-garage";


export class ParkingGarageDto extends TinyType {
    parkingSpaces: ParkingSpacesDto;
    public toParkingGarage = () => new ParkingGarage(new UUID(this.id), this.day, this.capacity, this.free);

    public static fromParkingGarage = (parkingGarage: ParkingGarage) => new ParkingGarageDto(
        parkingGarage.id.id,
        parkingGarage.day,
        parkingGarage.capacity,
        parkingGarage.free
    );

    public static fromJSON = (o: Serialised<ParkingGarage>) => new ParkingGarage(
        o.id as string,
        o.day as string,
        o.capacity as number,
        o.free as number
    );

    constructor(public readonly id: string, public readonly day: string, public readonly capacity: number, public readonly free: number) {
        super();
        this.parkingSpaces = new ParkingSpacesDto(new CapacityDto(capacity), new CapacityDto(free));
    }

}

class ParkingSpacesDto extends TinyType {
    public static fromJSON = (o: Serialised<ParkingSpacesDto>) => new ParkingSpacesDto(
        CapacityDto.fromJSON(o.capacity as any),
        CapacityDto.fromJSON(o.free as any),
    );

    constructor(public readonly capacity: CapacityDto, public free: CapacityDto) {
        super();
    }
}

class CapacityDto extends TinyType {
    public static fromJSON = (o: Serialised<CapacityDto>) => new CapacityDto(
        o.capacity as number,
    );

    constructor(public readonly capacity: number) {
        super();
        // TODO ensure()
    }
}
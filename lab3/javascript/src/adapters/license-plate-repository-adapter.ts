import {DynamoDB} from 'aws-sdk';
import {LicensePlateRepository} from "../domain/LicensePlateService/license-plate-repository";
import {LicensePlateRegistration, LicensePlateType} from "../domain/LicensePlateService/license-plate-registration";
import {LicensePlateRegistrationDto} from "./license-plate-registration-dto";

const documentClient: DynamoDB.DocumentClient = new DynamoDB.DocumentClient();

async function resolveTableName(): Promise<string> {
    if (!process.env.LICENSE_PLATE_TABLE_NAME) {
        return Promise.reject('env property LICENSE_PLATE_TABLE_NAME not found.');
    }
    return process.env.LICENSE_PLATE_TABLE_NAME.toString();
}

export class LicensePlateRepositoryAdapter implements LicensePlateRepository{

    public async findLicensePlate(license: string): Promise<LicensePlateRegistration> {
        console.log("findLicensePlate with "+license);
        let tableName = await resolveTableName();
        console.log("tableName: "+tableName);
        let params = {
            TableName: tableName,
            KeyConditionExpression: '#license = :license',
            ExpressionAttributeNames: {
                '#license': 'license',
            },
            ExpressionAttributeValues: {
                ':license': license,
            }
        };

        return documentClient.query(params).promise().then(
            data => {
                console.log("data: "+data.Count);
                if (data.Count && data.Count > 0 && data.Items) {
                    console.log("data.items");
                    let licensePlateRegistrationDTO: LicensePlateRegistrationDto = LicensePlateRegistrationDto.fromJSON(data.Items.pop() as any);
                    return Promise.resolve(licensePlateRegistrationDTO.toLicensePlateRegistration());
                }
                console.log("nothing...");
                return Promise.resolve(new LicensePlateRegistration(license, LicensePlateType.UNKNOWN));
            },
            error => {
                return Promise.reject(error.message)
            }
        );

    }

}
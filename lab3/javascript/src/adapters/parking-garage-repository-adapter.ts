import {DynamoDB} from 'aws-sdk';
import {PutItemInput, PutItemInputAttributeMap} from 'aws-sdk/clients/dynamodb';
import {ParkingGarage} from "../domain/ParkingGarage/parking-garage";
import {ParkingGarageRepository} from "../domain/ParkingGarage/parking-garage-repository";
import {ParkingGarageDto} from "./parking-garage-dto";


const documentClient: DynamoDB.DocumentClient = new DynamoDB.DocumentClient();

async function resolveTableName(): Promise<string> {
    if (!process.env.PARKING_GARAGE_TABLE_NAME) {
        return Promise.reject('env property PARKING_GARAGE_TABLE_NAME not found.');
    }
    return process.env.PARKING_GARAGE_TABLE_NAME.toString();
}

export class ParkingGarageRepositoryAdapter implements ParkingGarageRepository {

    public async saveParkingGarage(parkingGarage: ParkingGarage): Promise<ParkingGarage> {
        const tableName = await resolveTableName();
        const params: PutItemInput = {
            TableName: tableName,
            Item: ParkingGarageDto.fromParkingGarage(parkingGarage).toJSON() as PutItemInputAttributeMap,
        };
        return documentClient.put(params).promise().then(
            data => Promise.resolve(parkingGarage),
            error => Promise.reject(error)
        );

    }

    public async findParkingGarage(id: string): Promise<ParkingGarage> {
        const tableName = await resolveTableName();

        const params = {
            TableName: tableName,
            KeyConditionExpression: '#id = :id',
            ExpressionAttributeNames: {
                '#id': 'id',
            },
            ExpressionAttributeValues: {
                ':id': id,
            }
        };

        let data = await documentClient.query(params).promise();
        if (data.Items) {
            let parkingGarageDto: ParkingGarageDto = ParkingGarageDto.fromJSON(data.Items.pop() as any);

            return Promise.resolve(parkingGarageDto.toParkingGarage());
        }
        return Promise.reject('no parking garage found');

    }
}
import {TinyType} from "tiny-types";


export class NoLicensePlateMatched {

    type: string = 'NoLicensePlateMatched';

    constructor(public readonly number: string) {
    }
}

export class AppointmentLicensePlateMatched {

    type: string = 'AppointmentLicensePlateMatched';

    constructor(public readonly number: string) {
    }
}

export class EmployeeLicensePlateMatched {

    type: string = 'EmployeeLicensePlateMatched';

    constructor(public readonly number: string) {
    }
}
import {Serialised, TinyType} from "tiny-types";
import {LicensePlateRegistration, LicensePlateType} from "../domain/LicensePlateService/license-plate-registration";

export class LicensePlateRegistrationDto extends TinyType {
    public toLicensePlateRegistration = () => new LicensePlateRegistration(this.license, LicensePlateType[this.type.toString()]);

    public static fromJSON = (o: Serialised<LicensePlateRegistrationDto>) => new LicensePlateRegistrationDto(
        o.license as string,
        LicensePlateTypeDTO[o.type as string]
    );

    constructor(public readonly license: string, public readonly type: LicensePlateTypeDTO) {
        super();
    }
}

export enum LicensePlateTypeDTO {
    EMPLOYEE,
    APPOINTMENT,
    UNKNOWN
}
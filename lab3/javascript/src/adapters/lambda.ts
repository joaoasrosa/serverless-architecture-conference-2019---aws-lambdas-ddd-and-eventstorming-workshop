import {APIGatewayEvent, Callback, Context} from "aws-lambda";
import {DynamoDB} from 'aws-sdk';
import {resolveTableName} from "../properties";

const PARKING_CAPACITY: number = 200;
const documentClient: DynamoDB.DocumentClient = new DynamoDB.DocumentClient();


//FUNCTION
export function handle(event: APIGatewayEvent, context: Context, callback: Callback) {
    

}

class Response {
    private headers = {};
    private isBase64Encoded = false;

    public static OK = (body: any) => new Response(200, body);
    public static INTERNAL_SERVER_ERROR = () => new Response(500, "{ 'Error' : 'Internal Server Error' }");

    constructor(public readonly statusCode: number, public readonly body: string) {

    }
}
using System.Collections.Generic;
using System.Net;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(JsonSerializer))]

namespace Lambda
{
    public class Functions
    {
        /// <summary>
        /// A Lambda function to respond to HTTP Get methods from API Gateway
        /// </summary>
        /// <param name="request">The API Gateway request</param>
        /// <param name="context">The execution context</param>
        /// <returns>The number of available parking spots</returns>
        public APIGatewayProxyResponse Get(APIGatewayProxyRequest request, ILambdaContext context)
        {
            context.Logger.LogLine("Get the number of available parking spots.\n");

            uint numberOfAvailableParkingSpots = ParkingCapacity() - NumberOfParkedCars();

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(new
                {
                    numberOfAvailableParkingSpots
                }),
                Headers = new Dictionary<string, string> {{"Content-Type", "application/json"}}
            };

            return response;
        }

        private static uint ParkingCapacity()
        {
            return 200;
        }

        private static uint NumberOfParkedCars()
        {
            using (var dynamoDbClient = CreateDynamoDbClient())
            {
                using (var dynamoDbContext = new DynamoDBContext(dynamoDbClient))
                {
                    IEnumerable<ScanCondition> scanConditions = new[]
                    {
                        new ScanCondition(nameof(ParkingSpot.IsAvailable), ScanOperator.Equal, false)
                    };

                    var usedParkingSpots = dynamoDbContext
                        .ScanAsync<ParkingSpot>(scanConditions, new DynamoDBOperationConfig {ConsistentRead = true})
                        .GetRemainingAsync().Result;

                    return (uint)usedParkingSpots.Count;
                }
            }
        }

        private static IAmazonDynamoDB CreateDynamoDbClient()
        {
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddEnvironmentVariables();

            var configuration = configurationBuilder.Build();

            var options = configuration.GetAWSOptions();
            return options.CreateServiceClient<IAmazonDynamoDB>();
        }
    }
}

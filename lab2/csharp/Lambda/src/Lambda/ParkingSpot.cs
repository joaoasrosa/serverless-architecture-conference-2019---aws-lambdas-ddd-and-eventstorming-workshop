using Amazon.DynamoDBv2.DataModel;

namespace Lambda
{
    [DynamoDBTable("ParkingSpots")]
    public class ParkingSpot
    {
        [DynamoDBHashKey] public ushort Number { get; set; }

        [DynamoDBProperty] public bool IsAvailable { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Net;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.TestUtilities;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace Lambda.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestGetMethod()
        {
            CreateData();
            SetEnvironmentVariables();

            Functions functions = new Functions();

            var request = new APIGatewayProxyRequest();
            var context = new TestLambdaContext();
            var response = functions.Get(request, context);

            Assert.Equal(200, response.StatusCode);
            Assert.Equal("{\"numberOfAvailableParkingSpots\":157}", response.Body);
        }

        private void CreateData()
        {
            using (var dynamoDbClient = CreateDynamoDbClient())
            {
                const string tableName = "ParkingSpots";

                var tables = dynamoDbClient.ListTablesAsync().Result;
                if (tables.TableNames.Count > 0)
                {
                    var deleteTableResponse = dynamoDbClient.DeleteTableAsync(tableName).Result;
                    if (deleteTableResponse.HttpStatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception($"Can't delete the DynamoDB table ${tableName}.");
                    }
                }

                dynamoDbClient.CreateTableAsync(tableName,
                    new List<KeySchemaElement>
                    {
                        new KeySchemaElement(nameof(ParkingSpot.Number), KeyType.HASH)
                    },
                    new List<AttributeDefinition>
                    {
                        new AttributeDefinition(nameof(ParkingSpot.Number), ScalarAttributeType.N)
                    },
                    new ProvisionedThroughput(5, 5));

                using (var dynamoDbContext = new DynamoDBContext(dynamoDbClient))
                {
                    for (ushort i = 1; i <= 200; i++)
                    {
                        var parkingSpot = new ParkingSpot
                        {
                            Number = i,
                            IsAvailable = i > 43
                        };

                        dynamoDbContext.SaveAsync(parkingSpot).Wait();
                    }
                }
            }
        }

        private static IAmazonDynamoDB CreateDynamoDbClient()
        {
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile("appsettings.json");

            var configuration = configurationBuilder.Build();

            var options = configuration.GetAWSOptions();
            return options.CreateServiceClient<IAmazonDynamoDB>();
        }

        private static void SetEnvironmentVariables()
        {
            Environment.SetEnvironmentVariable("AWS__ServiceURL", "http://localhost:8000");
        }
    }
}

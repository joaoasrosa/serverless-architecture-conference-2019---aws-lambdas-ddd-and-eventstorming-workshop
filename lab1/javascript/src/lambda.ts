import {APIGatewayEvent, Callback, Context} from "aws-lambda";

const PARKING_CAPACITY: number = 200;
const NUMBER_OF_PARKED_CARS: number = 43;

//FUNCTION
export function handle(event: APIGatewayEvent, context: Context, callback: Callback) {

    let numberOfAvailableParkingSpots = {
        'numberOfAvailableParkingSpots' : PARKING_CAPACITY - NUMBER_OF_PARKED_CARS
    };

    return callback(null, Response.OK(numberOfAvailableParkingSpots));
}

class Response {
    private headers = {};
    private isBase64Encoded = false;

    public static OK = (body: any) => new Response(200, body);

    constructor(public readonly statusCode: number, public readonly body: string) {

    }
}
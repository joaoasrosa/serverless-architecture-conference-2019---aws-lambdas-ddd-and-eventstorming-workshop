import {handle} from "../src/lambda";
import {APIGatewayEvent} from "aws-lambda";

const EMPTY_BODY: any = {};
const EMPTY_CONTEXT: any = {};


describe('test lambda', () => {
    it('should return 157', done => {
        handle(EMPTY_BODY as APIGatewayEvent, EMPTY_CONTEXT, (err, data) => {
            console.log(data);
            expect(err).toBeNull();
            expect(data).toBeDefined();
            expect(data.statusCode).toBe(200);
            expect(data.body.numberOfAvailableParkingSpots).toBe(157);
            done();
        });
    });
});